'use client'

import Image from "next/image";
import styles from "@/app/page.module.css";

import {useState} from "react";
import axios from "axios";


export default function Page() {
    const [question, setQuestion] = useState('');
    const [response, setResponse] = useState('');


    const handleQuestion = e => {
        setQuestion(e.target.value);
    }

    const handleSubmit = e => {
        e.preventDefault();


        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyAXA9REfGoDBdXsIeBlaw9J5QrdzJa7zqU'

        const data = {
            "contents": [
                {
                    "role": "user",
                    "parts": [
                        {
                            "text": "너는 해충 전문가야 만약 해충의 이름이나 특징을 알려주는 글을 보면 무슨 해충인지 알려주고 해충퇴치제가 없을 때 해충을 퇴치하는 방법을 알려주면 돼"
                        },
                        {
                            "text": ""
                        }
                    ]
                },
                {
                    "role": "user",
                    "parts": [
                        {
                            "text": question
                        }
                    ]
                }
            ]
        }

        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
            })
            .catch(err => {
                console.log('데이터 제대로 못받음');
            })
    }




    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div className="jo-div">
                    <div>박멸하고 싶은 해충은 ?</div>
                </div>
                <input type="text" value={question} onChange={handleQuestion} placeholder="만들어달라 하거나 먼저 이야기를 시작하세요"/>
                <button type="submit">확인</button>
            </form>
            {/* && 연산자이기 때문에 앞 리스폰스가 참이어야지 뒤에가 보인다 값이 없으면 바로 끝남*/}
            {response && <p>{response}</p>}
        </div>
    );
}
